/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btp.makeexcelforjava;

/**
 *
 * @author UU088151
 */
public class Employee {

    private String name;
    private String skillLevel;
    private int salary;
    private String title;

    public Employee() {
    }

    public Employee(String name, String skillLevel, int salary, String title) {
        this.name = name;
        this.skillLevel = skillLevel;
        this.salary = salary;
        this.title = title;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(String skillLevel) {
        this.skillLevel = skillLevel;
    }

    public int getSalary() {
        return salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    
    
}
