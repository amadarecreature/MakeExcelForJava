/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btp.makeexcelforjava;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jett.transform.ExcelTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author UU088151
 */
public class JettTool {

    private static final String IN_DIR = "D:/Users/UU088151/Documents/NetBeansProjects/MakeExcelForJava/resources/";
    private static final String OUT_DIR = "D:/Users/UU088151/Documents/NetBeansProjects/MakeExcelForJava/output";

    public static void main(String[] args) throws InvalidFormatException, IOException {
        //        JettTool.makeLoop();
        //        makeShift_bean();
        //        sample();
        //        makeShift_beans();
        makeLoop();

    }

    public static File makeShift_bean() throws InvalidFormatException, IOException {
        System.out.println(System.getProperty("user.dir"));

        Map<String, Object> beans = new HashMap();
        Employee emp1 = new Employee();
        emp1.setName("社員1");
        emp1.setSalary(100);
        emp1.setSkillLevel("A");
        Employee emp2 = new Employee();
        emp2.setName("社員2");
        emp2.setSalary(10000);
        emp2.setSkillLevel("E");

        beans.put("employee1", emp1);
        beans.put("employee2", emp2);

        String inPath = IN_DIR + "template_bean.xlsx";
        String outPath = OUT_DIR + "OutPutBean.xlsx";

        try {

            ExcelTransformer transformer = new ExcelTransformer();
            transformer.transform(inPath, outPath, beans);

        } catch (IOException e) {
            System.err.println("IOException reading " + inPath + ": " + e.getMessage());
            throw e;
        } catch (InvalidFormatException e) {
            System.err.println("InvalidFormatException reading " + inPath + ": " + e.getMessage());
            throw e;
        }

        return new File(outPath);
    }

    public static void makeShift_beans() throws InvalidFormatException, IOException {
        System.out.println(System.getProperty("user.dir"));

        Map<String, Object> beans = new HashMap();
        Employee emp1 = new Employee("社員1", "A", 11000, "ホームラン王");

        Employee emp2 = new Employee("社員2", "B", 22000, "盗塁王");

        List list = new ArrayList();
        list.add(emp1);
        list.add(emp2);

        beans.put("sampleBeanList", list);

        String inPath = IN_DIR + "template_beans.xlsx";
        String outPath = "D:/Users/UU088151/Documents/NetBeansProjects/MakeExcelForJava/OutPutBeans.xlsx";

        try {

            ExcelTransformer transformer = new ExcelTransformer();
            transformer.transform(inPath, outPath, beans);

        } catch (IOException | InvalidFormatException e) {
            throw e;
        }

    }

    public static void makeLoop() throws InvalidFormatException, IOException {
        System.out.println(System.getProperty("user.dir"));
        Map<String, Object> beans = new HashMap();

        List<Employee> employees = Lists.newArrayList();

        Employee emp1 = new Employee("名前1", "A41", 1500, "ホームラン");
        employees.add(emp1);

        Employee emp2 = new Employee("名前222", "C51", 2000, "ホームラン");
        employees.add(emp2);

        beans.put("employees", employees);

        String inPath = IN_DIR + "loop1_template.xlsx";
        String outPath = OUT_DIR + "loop1_output.xlsx";

        try {

            ExcelTransformer transformer = new ExcelTransformer();
            transformer.transform(inPath, outPath, beans);

        } catch (IOException | InvalidFormatException e) {
            throw e;
        }

    }

    public static void sample() {
        Map beans = new HashMap();
        List list = new ArrayList();
        for (int i = 0; i < 10; i++) {
            list.add(new SampleBean("サンプル" + i, i + 1));
        }

        String inPath = IN_DIR + "template3.xlsx";
        String outPath = OUT_DIR + "resultsample.xlsx";

        beans.put("sampleBeanList", list);
        ExcelTransformer transformer = new ExcelTransformer();
        try {
            transformer.transform(inPath, outPath, beans);
        } catch (IOException | InvalidFormatException e) {
            Logger.getLogger(JettTool.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
