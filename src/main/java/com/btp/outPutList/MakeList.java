/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.btp.outPutList;

import com.btp.makeexcelforjava.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.sf.jett.transform.ExcelTransformer;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

/**
 *
 * @author UU088151
 */
public class MakeList {

    private static final String IN_DIR = "D:/Users/UU088151/Documents/NetBeansProjects/MakeExcelForJava/resources/";
    private static final String OUT_DIR = "D:/Users/UU088151/Documents/NetBeansProjects/MakeExcelForJava/output/";

    public static void main(String[] args) throws InvalidFormatException, IOException {
        //        MakeList.makeLoop();
        //        makeShift_bean();
        //        sample();
        //        makeShift_beans();
        makeExcel("表作成");

    }

    public static File makeExcel(String fileName) throws InvalidFormatException, IOException {
        System.out.println(System.getProperty("user.dir"));

        String inPath = IN_DIR + "template_"  + fileName + ".xlsx";
        String outPath = OUT_DIR + fileName + ".xlsx";

        Map<String, Object> outPutInfo = new HashMap();

        List<String> subList = new ArrayList();
        subList.add("1000");
        subList.add("2000");
        subList.add("3000");
        subList.add("4000");
        subList.add("5000");

        outPutInfo.put("LIST", subList);
        
        try {

            ExcelTransformer transformer = new ExcelTransformer();
            transformer.transform(inPath, outPath, outPutInfo);

        } catch (IOException e) {
            System.err.println("IOException reading " + inPath + ": " + e.getMessage());
            throw e;
        } catch (InvalidFormatException e) {
            System.err.println("InvalidFormatException reading " + inPath + ": " + e.getMessage());
            throw e;
        }

        return new File(outPath);
    }


}
